import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import { asyncRouterMap, constantRouterMap } from '@/router'
import { findUserMenu } from '../request/api';
/**
 * 动态权限判断
 * @param menuIds  用户拥有的菜单列表
 * @param route   菜单选择
 * @returns {boolean}
 */
function hasPermission(menuIds, route) {
    let result = false
    menuIds.filter(menuId => {
        if (menuId === route.menuId) {
            result = true
        }
    })
    return result
}
import { Form } from 'element-ui';

/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param asyncRouterMap
 * @param roles
 */
function filterAsyncRouter(asyncRouterMap, menuIds) {
    const accessedRouters = asyncRouterMap.filter(route => {
        if (hasPermission(menuIds, route)) {
            if (route.children && route.children.length) {
                route.children = filterAsyncRouter(route.children, menuIds)
            }
            return true
        }
        return false
    })
    return accessedRouters
}

const store = new Vuex.Store({
    state: {
        routers: constantRouterMap,
        addRouters: [],
        roles: []
    },
    getters: {
        permission_routers: state => state.routers,
        addRouters: state => state.addRouters,
        roles: state => state.roles
    },
    mutations: {
        SET_ROUTERS: (state, routers) => {
            state.addRouters = routers
            state.routers = constantRouterMap.concat(routers)
            // console.log(constantRouterMap.concat(routers))
            // console.log(state.routers)
        },
        SET_INFO: (state, roles) => {
            // console.log(roles)
            state.roles = roles;
        }
    },
    actions: {
        // GenerateRoutes({ commit }, data) {
        GenerateRoutes({ commit }) {
            return new Promise(resolve => {
                // const { roles } = data
                let accessedRouters
                // fetchUserTree().then(response => {
                //     if (roles.indexOf('admin') >= 0) {
                //         accessedRouters = asyncRouterMap
                //     } else {
                //         accessedRouters = filterAsyncRouter(asyncRouterMap, response.data)
                //     }
                //     commit('SET_ROUTERS', accessedRouters)
                //     resolve()
                // })
                // let Ids = [1,2,4,5,6,7,8,9,11,12,13,25,26,30,31,32]
                let Ids = [];
                findUserMenu({
                }).then(res => {
                    if (res.data.ret == 1) {
                        Ids = res.data.data
                        accessedRouters = filterAsyncRouter(asyncRouterMap, Ids)
                        commit('SET_ROUTERS', accessedRouters)
                        resolve()
                    } else {
                        this.$message.error(res.data.msg)
                    }
                }).catch(err => {

                })
                // if (roles.indexOf('admin') >= 0) {
                //      accessedRouters = asyncRouterMap
                // } else {
                //      accessedRouters = filterAsyncRouter(asyncRouterMap, Ids)
                // }

            })
        },
        GetInfo({ commit }) {
            return new Promise(resolve => {
                let roles = [1, 2, 3, 4, 5];
                commit('SET_INFO', roles)
                resolve()
            })
        }
    }
})

export default store;