import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home/Home'
import login from '@/components/login/login'
import Index from '@/components/Index'
import error from '@/components/error/404'
//销售业务
import cardManage from '@/components/sellBusiness/cardManage'
import clientBind from '@/components/sellBusiness/clientBind'
//销售查询
import sellData from '@/components/sellInquire/sellData'
import sellDetail from '@/components/sellInquire/sellDetail'
import truckInquire from '@/components/sellInquire/truckInquire'
import sellSummarize from '@/components/sellInquire/sellSummarize'
//系统管理
import jurisdictionManage from '@/components/systemManage/jurisdictionManage'
import noticeManage from '@/components/systemManage/noticeManage'
import roleManage from '@/components/systemManage/roleManage'
import userManage from '@/components/systemManage/userManage'
import factoryManage from '@/components/systemManage/factoryManage'
//采购业务
import purchaseBusiness from '@/components/purchaseBusiness/purchaseBusiness'
//采购报表
import purchaseForm from '@/components/purchaseForm/purchaseForm'
//我的信息
import accountFunds from '@/components/myInfo/accountFunds'
import deliveryDetail from '@/components/myInfo/deliveryDetail'
import pickSummary from '@/components/myInfo/pickSummary'
import selfCredit from '@/components/myInfo/selfCredit'
import selfPay from '@/components/myInfo/selfPay'
import sellOrder from '@/components/myInfo/sellOrder'
//财务管理
import customerCredit from '@/components/financialManage/customerCredit'
import customerStatement from '@/components/financialManage/customerStatement'
import invoiceRegister from '@/components/financialManage/invoiceRegister'
import invoiceStatements from '@/components/financialManage/invoiceStatements'
import loanFor from '@/components/financialManage/loanFor'
import loanRecovery from '@/components/financialManage/loanRecovery'

Vue.use(Router)


export const constantRouterMap = [
  // 不需要登陆
  { path: '/', redirect: '/login', component: login, hidden: true },
  { path: '/login', component: login, name: 'login', hidden: true },
  { path: '/404', component: error, name: 'error', hidden: true },
  {
    path: '/',
    component: home,
    name: '',
    hidden: false,
    iconCls: 'el-icon-s-home',
    leaf: true,
    children: [
      { path: '/index', component: Index, name: '首页'}
    ]
  },
]

export default new Router({
  mode: 'history',
  base: "/dlweb",
  routes: constantRouterMap
})

export const asyncRouterMap = [
  {
    menuId: 9,
    path: "/",
    hidden: false,
    component: home,
    name: "系统管理",
    iconCls: "el-icon-menu",
    children: [
      // {
      //   menuId: 10, path: "/systemManage/jurisdictionManage", component: jurisdictionManage, name: "权限管理", meta: {
      //     keepAlive: true
      //   }
      // },
      {
        menuId: 11, path: "/systemManage/noticeManage", component: noticeManage, name: "公告管理", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 12, path: "/systemManage/roleManage", component: roleManage, name: "角色管理", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 13, path: "/systemManage/userManage", component: userManage, name: "用户管理", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 32, path: "/systemManage/factoryManage", component: factoryManage, name: "工厂管理", meta: {
          keepAlive: true
        }
      }
    ]
  },
  {
    menuId: 1,
    path: "/",
    hidden: false,
    component: home,
    name: "销售业务",
    iconCls: "el-icon-menu",
    children: [
      {
        menuId: 2, path: "/sellBusiness/cardManage", component: cardManage, name: "纸卡管理", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 3, path: "/sellBusiness/clientBind", component: clientBind, name: "客户绑定", meta: {
          keepAlive: true
        }
      }
    ]
  },
  {
    menuId: 4,
    path: "/",
    hidden: false,
    component: home,
    name: "销售查询",
    iconCls: "el-icon-menu",
    children: [
      {
        menuId: 5, path: "/sellInquire/sellData", component: sellData, name: "销售数据分析", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 6, path: "/sellInquire/sellDetail", component: sellDetail, name: "销售明细", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 7, path: "/sellInquire/truckInquire", component: truckInquire, name: "待装车辆查询", meta: {
          keepAlive: true
        }
      },
      {
        menuId: 8, path: "/sellInquire/sellSummarize", component: sellSummarize, name: "销售汇总", meta: {
          keepAlive: true
        }
      }
    ]
  },
  {
    menuId: 14,
    path: '/',
    component: home,
    name: '',
    hidden: false,
    iconCls: 'el-icon-menu',
    leaf: true,
    children: [
      { menuId: 15, path: '/purchaseBusiness', component: purchaseBusiness, name: '采购业务' }
    ]
  },
  {
    menuId: 16,
    path: '/',
    component: home,
    name: '',
    hidden: false,
    iconCls: 'el-icon-menu',
    leaf: true,
    children: [
      { menuId: 17, path: '/purchaseForm', component: purchaseForm, name: '采购报表' }
    ]
  },
  {
    menuId: 18,
    path: '/',
    component: home,
    name: '我的信息',
    hidden: false,
    iconCls: 'el-icon-menu',
    children: [
      { menuId: 19, path: '/myInfo/accountFunds', component: accountFunds, name: '账户资金查询' },
      { menuId: 20, path: '/myInfo/deliveryDetail', component: deliveryDetail, name: '提货明细查询' },
      { menuId: 21, path: '/myInfo/pickSummary', component: pickSummary, name: '提货汇总查询' },
      { menuId: 22, path: '/myInfo/selfCredit', component: selfCredit, name: '自助授信查询' },
      { menuId: 23, path: '/myInfo/selfPay', component: selfPay, name: '自助付款查询' },
      { menuId: 24, path: '/myInfo/sellOrder', component: sellOrder, name: '销售订单查询' }
    ]
  },
  {
    menuId: 25,
    path: '/',
    component: home,
    name: '财务管理',
    hidden: false,
    iconCls: 'el-icon-menu',
    children: [
      {
        menuId: 26, path: '/financialManage/customerCredit', component: customerCredit, name: '客户授信查询', meta: {
          keepAlive: true
        }
      },
      {
        menuId: 27, path: '/financialManage/customerStatement', component: customerStatement, name: '客户对账单', meta: {
          keepAlive: true
        }
      },
      {
        menuId: 28, path: '/financialManage/invoiceRegister', component: invoiceRegister, name: '发票登记', meta: {
          keepAlive: true
        }
      },
      {
        menuId: 29, path: '/financialManage/invoiceStatements', component: invoiceStatements, name: '发票报表', meta: {
          keepAlive: true
        }
      },
      {
        menuId: 30, path: '/financialManage/loanFor', component: loanFor, name: '货款查询', meta: {
          keepAlive: true
        }
      },
      {
        menuId: 31, path: '/financialManage/loanRecovery', component: loanRecovery, name: '货款回收', meta: {
          keepAlive: true
        }
      }
    ]
  }
]

