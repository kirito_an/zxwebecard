import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css'// Progress 进度条样式
import { Message } from 'element-ui'
// import { getToken,removeToken } from '@/utils/auth' // 验权
let getToken = '';
const whiteList = '/login/invoice/contractAdd' // 不重定向白名单
// const whiteList = '' // 不重定向白名单
router.beforeEach((to, from, next) => { // 开启Progress
  var params =to.query
  var ticket = params.ticket
  var code = params.code
  if(null != ticket && null != code){
    next()
  }else{
  NProgress.start()
  // if (getToken()) { // 判断是否有token
  if (sessionStorage.getItem('key')) { // 判断是否有token
    // console.log(store.getters.roles.length)
      if (store.getters.roles.length === 0) { // 判断当前用户是否已拉取完user_info信息
        // console.log('333')
        // store.dispatch('GetInfo').then(res => { // 拉取用户信息
        //   const roles = res.data.data.roles
        //   store.dispatch('GenerateRoutes', { roles }).then(() => { // 生成可访问的路由表
        //     router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
        //     next({ ...to }) // hack方法 确保addRoutes已完成
        //   })
        // }).catch((e) => {
        //   store.dispatch('FedLogOut').then(() => {
        //     Message.error('验证失败,请重新登录')
        //     next({ path: '/login' })
        //   })
        // })
          store.dispatch('GetInfo').then(()=>{
            store.dispatch('GenerateRoutes').then(() => { // 生成可访问的路由表
              router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
              next({ ...to }) // hack方法 确保addRoutes已完成
            })
          })
      }else{
        next()
      }
  } else {
    // console.log(to.path)
    if(to.path=='/login'){//如果是登录页面路径，就直接next()
      next();
    }else{//不然就跳转到登录；
      next('/login');
      NProgress.done()
    }
  }
}
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
