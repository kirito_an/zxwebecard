import { get, post } from './http';

export const login = p => post('/login', p); // 登录
export const billDetail = p => post('/bill/billDetail', p); // 明细
export const sBillSumPage = p => post('/sBillSum/sBillSumPage', p); // 汇总
export const findValidFactory = p => post('/factory/findValidFactory', p); // 工厂查询
export const findDLStockName = p => post('/sysDict/findDLStockName', p); // 物料
export const findSalesAnalysisEchars = p => post('/salesAnalysis/findSalesAnalysisEchars', p);
export const financeList = p => post('/finance/financeList', p);// 查询货款数据
export const findCustomer = p => post('/finance/findCustomer', p);// 获取指定客户信息
export const querySalesManList = p => post('/finance/querySalesManList', p);// 获取工厂业务员列表
export const queryPayTypeList = p => post('/finance/queryPayTypeList', p);// 获取工厂付款方式列表
export const customerPayment = p => post('/finance/customerPayment', p);// 客户回款
export const saveNotice = p => post('/notice/saveNotice', p);// 新增公告
export const delNotice = p => post('/notice/delNotice', p);// 删除公告
export const editNotice = p => post('/notice/editNotice', p);// 编辑公告
export const pubNotice = p => post('/notice/pubNotice', p);// 发布公告
export const noticePager = p => post('/notice/noticePager', p);// 公告列表
export const getPublishNotice = p => post('/notice/certified/getPublishNotice', p);// 展示公告
export const findUserMenu = p => post('/permissionMenu/certified/findUserMenu', p);// 查询用户菜单列表
export const permissionTree = p => post('/permissionMenu/certified/permissionTree', p);// 查询全部权限树
export const insertAndConfRole = p => post('/user/insertAndConfRole', p);// 新增角色
export const rolePage = p => post('/user/rolePage', p);// 角色分页列表
export const prohibitRole = p => post('/user/prohibitRole', p);// 禁用角色
export const getRolePermission = p => post('/permissionMenu/certified/getRolePermission', p);// 查询角色权限
export const getUserPermissionTree = p => post('/permissionMenu/certified/getUserPermissionTree', p);// 查看角色权限
export const batchConfRolePermission = p => post('/permissionMenu/batchConfRolePermission', p);// 批量配置角色权限
export const getUserPermissionCode = p => post('/permissionMenu/certified/getUserPermissionCode', p);// 查询菜单权限码
export const queryVehicLoaded = p => post('/delivery/queryVehicLoaded', p);// 查询工厂待装车辆
export const factoryList = p => post('/factory/list', p);// 分页查询工厂数据信息
export const factoryGetById = p => post('/factory/getById', p);// 通过工厂Id获取工厂数据信息
export const factoryUpdate = p => post('/factory/update', p);// 新增修改工厂数据信息
export const factoryUpdateStatus = p => post('/factory/updateStatus', p);// 修改工厂状态

export const customerCreditDetail = p => post('/finance/customerCreditDetail', p);// 获取指定客户授信详情
export const customerCredit = p => post('/finance/customerCredit', p);// 客户授信提交
export const list = p => post('/paperCard/list', p);// 分页查询纸卡数据信息
export const findDtl = p => post('/paperCard/findDtl', p);// 查询纸卡详细数据
export const findAllClient = p => post('/deliveryClient/findAllClient', p);// 查询纸卡详细数据
export const findContract = p => post('/delivery/findContract', p);// 查询纸卡详细数据
export const findContractExt = p => post('/delivery/findContractExt', p);// 查询客户合同详情信息
export const createPaperCard = p => post('/paperCard/createPaperCard', p);// 办理纸卡
export const customerCreditList = p => post('/finance/customerCreditList', p);// 分页客户授信列表数据
export const userPage = p => post('/user/userPage', p);// 用户分页列表
export const editPassword = p => post('/user/certified/editPassword', p);// 修改用户名密码
export const editRoleInfo = p => post('/user/editRoleInfo', p);// 修改角色信息
export const resetPassword = p => post('/user/resetPassword', p);// 重置密码
export const confUserRole = p => post('/user/confUserRole', p);// 用户角色配置
export const saveAndConfFactory = p => post('/user/saveAndConfFactory', p);// 新增用户



















export const checkRoleName = p => post('/user/certified/checkRoleName', p);// 校验用户名



