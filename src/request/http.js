import axios from 'axios';
import Qs from 'qs';
import { MessageBox } from 'element-ui';
import router from '../router/index.js';



// axios.defaults.baseURL='http://192.168.2.209:8081/zjDeliveryWeb';  // 赵文豪
//  axios.defaults.baseURL='http://192.168.2.187:8081/zjDeliveryWeb';  // 何煊烁
// axios.defaults.baseURL='http://192.168.2.113:8081/zjDeliveryWeb'; //  韩锐钊
axios.defaults.baseURL='http://192.168.0.200:8081/zjDeliveryWeb';  // 200服务器
// axios.defaults.baseURL='http://118.89.157.37:8081/zjDeliveryWeb';  //
axios.defaults.timeout = 10000; //设置请求超时时间

//post请求头的设置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// axios.defaults.headers['Authorization'] = sessionStorage.getItem('key');
// 发送拦截器
axios.interceptors.request.use(config => {
    if(sessionStorage.getItem('key')){
        config.headers['Authorization'] = sessionStorage.getItem('key');
    }
    return config
  }, error => {
    // Do something with request error
    Promise.reject(error)
  })

//响应拦截器
axios.interceptors.response.use(
    response => {
        if (response.status === 200) {
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },
    //服务端状态码不是200的情况
    error => {
        if (error.response.status) {
            switch (error.response.status) {
                //比如401 未登录
                case 401:
                    // router.replace({
                    // })
                    break;
                //比如403登录过期
                case 403:
                    // MessageBox.alert('请重新登录', '提示', {
                    //     confirmButtonText: '确定'
                    // }).then(()=>{
                    //     sessionStorage.removeItem('key');
                    //     router.replace({
                    //         path:"/index"
                    //     });
                    // })                  
                // 比如404请求不存在                
                case 404:
                    MessageBox.alert('网络请求不存在', '提示', {
                        confirmButtonText: '确定'
                    })
                    break;
                //一些其他的情况
                default:
                    MessageBox.alert(error.response.data.message, '提示', {
                        confirmButtonText: '确定'
                    })
            }
            return Promise.reject(error.response);
        }
    }
);

// get请求 url {String} [请求的url地址]  params {Object} [请求时携带的参数] 
export function get(url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            params: params
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err)
        })
    });
}

// post请求 
export function post(url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, Qs.stringify(params))
            .then(res => {
                resolve(res);
            }).catch(err => {
                reject(err);
            })
    });
}